#include "Overrides.h"
#include "Sideloader.h"
#include <dlfcn.h>

#define _cache_override(FN) Real::FN = reinterpret_cast<decltype(&::FN)>(dlsym(RTLD_NEXT, #FN));

namespace Relentless {
    namespace Real {
        decltype(&::rand) rand;
        decltype(&::time) time;
        decltype(&::ptrace) ptrace;
        decltype(&::dlsym) dlsym;
        decltype(&::dlvsym) dlvsym;
        decltype(&::dladdr) dladdr;
        decltype(&::dlopen) dlopen;
        decltype(&::dlclose) dlclose;
        decltype(&::dlerror) dlerror;
    };
    bool Override() {
        /*Real::rand = reinterpret_cast<decltype(&rand)>(dlsym(RTLD_NEXT, "rand"));
        Real::time = reinterpret_cast<decltype(&time)>(dlsym(RTLD_NEXT, "time"));
        Real::ptrace = reinterpret_cast<decltype(&ptrace)>(dlsym(RTLD_NEXT, "ptrace"));
        Real::ptrace = reinterpret_cast<decltype(&ptrace)>(dlsym(RTLD_NEXT, "dlsym"));
        Real::ptrace = reinterpret_cast<decltype(&)>(dlsym(RTLD_NEXT, "dlopen"));
        Real::ptrace = reinterpret_cast<decltype(&dlclose)>(dlsym(RTLD_NEXT, "dlclose"));
        Real::ptrace = reinterpret_cast<decltype(&dlerror)>(dlsym(RTLD_NEXT, "dlerror"));*/
        _cache_override(rand);
        _cache_override(time);
        _cache_override(ptrace);
        _cache_override(dlsym);
        _cache_override(dlvsym);
        _cache_override(dladdr);
        _cache_override(dlopen);
        _cache_override(dlclose);
        _cache_override(dlerror);

        Relentless::Sideload("relentless_dlfcn");

        return true;
    }

    struct ShimObject {
        ShimObject() {
            Override();
        }
    } Shim _dalloc(1) __attribute__((used));

    bool IsUnrandomized = false, IsTimeFaked = false;
    time_t FakeNow = time_t();
    int FakeRandomValue = 0;

    bool Unrandomized() {
        return IsUnrandomized;
    }
    bool FakeTime() {
        return IsTimeFaked;
    }

    int RandomValue() {
        return FakeRandomValue;
    }
    time_t Now() {
        return FakeNow;
    }
};

long ptrace(enum __ptrace_request request, pid_t pid, void *addr, void *data) {
    _unused(addr, data);
    _unused(pid, request);
    return 0;
}

int rand() {
    if(Relentless::Unrandomized()) return Relentless::RandomValue();
    else {
        return (*Relentless::Real::rand)();
    }
}

time_t time(time_t* timer) {
    if(Relentless::FakeTime()) return Relentless::Now();
    else {
        return (Relentless::Real::time)(timer);
    }
}
