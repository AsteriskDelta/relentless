#ifndef RELENTLESS_SIDELOADER_H
#define RELENTLESS_SIDELOADER_H

namespace Relentless {
    void* Sideload(const std::string& path);
};

#endif
