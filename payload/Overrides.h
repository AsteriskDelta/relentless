#ifndef RELENTLESS_OVERRIDES_H
#define RELENTLESS_OVERRIDES_H
#include <ptrace.h>
#include <stdlib.h>
#include <dlfcn.h>

namespace Relentless {
    namespace Real {
        extern decltype(&::rand) rand;
        extern decltype(&::time) time;
        extern decltype(&::ptrace) ptrace;
        extern decltype(&::dlsym) dlsym;
        extern decltype(&::dlvsym) dlvsym;
        extern decltype(&::dladdr) dladdr;
        extern decltype(&::dlopen) dlopen;
        extern decltype(&::dlclose) dlclose;
        extern decltype(&::dlerror) dlerror;
    };
    bool Override();

    extern bool IsUnrandomized, IsTimeFaked;
    extern time_t FakeNow;
    extern int FakeRandomValue;

    bool Unrandomized();
    bool FakeTime();

    int RandomValue();
    time_t Now();
};

//long ptrace(enum __ptrace_request request, pid_t pid, void *addr, void *data);
//decltype(ptrace) ptrace;
//int rand();
//time_t time(time_t* timer);

#endif
