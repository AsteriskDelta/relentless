#include "Sideloader.h"
#include "Overrides.h"

namespace Relentless {
    void* Sideload(const std::string& path) {
        void *ret = Real::dlopen(path.c_str(), RTLD_NOW | RTLD_DEEPBIND);
        return ret;
    }
}
