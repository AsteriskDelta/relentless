#ifndef RELENTLESS_H
#define RELENTLESS_H
#include "Tracer.h"

namespace Relentless {
    typedef uint32_t EventIdx;
    struct EventTally {
        uint64_t occurrences;
    };
};

#endif
