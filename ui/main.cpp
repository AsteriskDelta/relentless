#include "Relentless.h"
#include "Capstone.h"
#include "Asm.h"
#include <ARKE/MessageQueue.h>
#include "Symbols.h"
#include "Symbol.h"

using namespace Relentless;
bool loop_ = true;
Tracer *tracer = nullptr;

struct Sample {
    uint64_t addr;
    size_t length;
    unsigned char data[16];
};
arke::MessageQueue<Sample> msgQueue = arke::MessageQueue<Sample>(8192);

void cleanup() {
        tracer->detach();
}

static void print_string_hex(unsigned char *str, size_t len)
{
	unsigned char *c;

	//printf("Code: ");
	for (c = str; c < str + len; c++) {
		printf("%02x ", *c & 0xff);
	}
	//printf("\n");
}

int analyzer() {
    //std::cout <<"STARTANALYZER\n";
    Capstone *disasm = new Capstone();
    disasm->initialize();

    while(loop_) {
        while(msgQueue.count() <= 0) std::this_thread::yield();
        Sample smp = msgQueue.pop();
        //std::cout <<"\tgot msg addr=" <<smp.addr <<"\n";

        std::cout << std::hex << smp.addr <<std::dec << " \t\t";
        print_string_hex(smp.data, smp.length);
        std::cout << "\t\t\t";
        Asm *sub = disasm->decode(smp.addr, smp.data, smp.length);

        if(sub != nullptr) {
            std::cout <<sub->begin()->str();
            delete sub;
        }
        std::cout <<"\n";
        // <<  << "\n";
        std::cout.flush();
    }
    return 0;
}

int main(int argc, char** argv) {
    if(argc < 2){
        std::cout << "Usage: " << argv[0] << " PID" << std::endl;
        exit(1);
    }

    //Capstone *disasm = new Capstone();
    //disasm->initialize();

    Spin::MemberInfo myInfo;
    myInfo.type = "relentless";

    Spin::Initialize(Spinster::MemberID(2, 1996), myInfo);

    Spinster::Local->thread(analyzer);

    uint32_t pid;
    if(std::isdigit(argv[1][0])) {
        pid = atoi(argv[1]);
    } else {
        char buf[512];
        std::string pname = std::string("pidof -s ") + std::string(argv[1]);
        FILE *cmd_pipe = popen(pname.c_str(), "r");

        fgets(buf, 512, cmd_pipe);
        pid = strtoul(buf, NULL, 10);

        pclose( cmd_pipe );
    }

    tracer = new Tracer();
    if(!tracer->attach(pid)) {
        std::cerr <<"Failed to attach to designated process ID" << std::endl;
        return 1;
    }
    std::atexit(cleanup);
    std::cout <<"Attached to " <<pid <<"\n";
    tracer->pause();

    Symbols *symbols = new Symbols();
    //symbols->load(tracer);
//sleep(1);
    //tracer->resume();
    //sleep(1);
    uint64_t prev = 0, cnt = 0;
    while(loop_) {

        tracer->resume();
        static struct timespec sleepTime, sleepRemaining;
        //sleepTime.tv_sec = 0;
        //sleepTime.tv_nsec = 1;
        //nanosleep(&sleepTime, &sleepRemaining);
        std::this_thread::yield();
        //usleep(100);
        tracer->pause();
        tracer->examine();
        //tracer->pause();
        uint64_t nptr = tracer->registers.rip;
        if(nptr != prev &&prev != 0x0) {
            cnt = 0;

            //tracer->step();
            tracer->guessInstr();
            Sample smp;
            smp.addr = nptr;
            smp.length = tracer->instr.current.size();
            memcpy(smp.data, tracer->instr.current.bytes, tracer->instr.current.size());
            //std::cout <<"sent msg addr=" <<smp.addr <<"\n";
            msgQueue.push(smp);
            //std::cout <<"\n";//std::hex <<nptr <<std::dec <<"\n";
        }
        prev = nptr;

        cnt++;
        //std::cout << std::hex <<nptr <<std::dec << " * " << cnt << " \t\t";
        //print_string_hex(tracer->instr.current.bytes, tracer->instr.current.size());

        //Asm *sub = disasm->decode(tracer->instr.current.begin, tracer->instr.current.bytes, tracer->instr.current.size());

        /*if(sub != nullptr) {
            std::cout <<sub->str();
            delete sub;
        }*/
        //std::cout <<"\r";
        // <<  << "\n";
        //std::cout.flush();
        //tracer->pause();

        //tracer->resume();
    }


    return 0;
}
