#ifndef RELENTLESS_OPERAND_H
#define RELENETLESS_OPERAND_H

namespace Relentless {
    class Register;

    class Operand {
    public:
        Operand();

        enum Type : char {
            Invalid = 0,
            Register = 1,
            Memory = 2,
            Literal = -1
        };
        Type type;
        union {
            struct {
                class Register *reg;
            } op;
            struct {
                Operand *segment;
                Operand *base;
                Operand *offset, *index, *scale, *displacement;
            } *mem;
            struct {
                uint64_t value;
            } lit;
        };
    protected:

    };
}

#endif
