#ifndef RELENTLESS_CAPSTONE_H
#define RELENTLESS_CAPSTONE_H

typedef size_t csh;
class cs_insn;

namespace Relentless {
    class Asm;

    class Capstone {
    public:
        bool initialize();

        Asm* decode(uint64_t addr, unsigned char *data, size_t length);
    protected:
        csh *handle;
    };
};

#endif
