#include "Capstone.h"
#include <capstone/capstone.h>
#include "Asm.h"

namespace Relentless {
    bool Capstone::initialize() {
        handle = new csh();
        if(cs_open(CS_ARCH_X86, CS_MODE_64, handle) != CS_ERR_OK) return false;
        else return true;
    }

    Asm* Capstone::decode(uint64_t addr, unsigned char *data, size_t length) {
        cs_insn *insn;
        size_t count = cs_disasm(*handle, data, length, addr, 0, &insn);
        if (count > 0) {
            Asm *ret = new Asm();
            size_t j;

            for (j = 0; j < count; j++) {
                //printf("0x%"PRIx64":\t%s\t\t%s\n", insn[j].address, insn[j].mnemonic, insn[j].op_str);
                ret->instr.push_back(Instruction());
                Instruction& ino = ret->instr.back();
                ino.mnemonic = std::string(insn[j].mnemonic);
                ino.string = std::string(insn[j].op_str);
                ino.addr = insn[j].address;
            }

            cs_free(insn, count);
            return ret;
        } else return nullptr;
    }
}
