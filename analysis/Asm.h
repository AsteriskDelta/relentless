#ifndef RELENTLESS_ASM_H
#define RELENTLESS_ASM_H
#include <list>
#include "Instruction.h"

namespace Relentless {
    class Asm {
    public:

        std::list<Instruction> instr;

        inline auto begin() {
            return instr.begin();
        }
        inline auto end() {
            return instr.end();
        }

        std::string str() const;
    protected:

    };
};

#endif
