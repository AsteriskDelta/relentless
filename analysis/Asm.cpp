#include "Asm.h"

namespace Relentless {
    std::string Asm::str() const {
        std::stringstream ret;
        bool first = true;
        
        for(auto& instruction : instr) {
            if(first) first = false;
            else ret <<"\n";

            ret <<  instruction.str();
        }

        return ret.str();
    }
}
