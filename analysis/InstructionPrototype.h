#ifndef RELENTLESS_INSTR_PROTO_H
#define RELENTLESS_INSTR_PROTO_H

namespace Relentless {
    class InstructionPrototype {
    public:

        std::string mnemonic;
        std::vector<uint8_t> bytes;

        struct {
            bool jmp : 1;
            bool cal : 1;
            bool ret : 1;
            bool cmp : 1;
            bool ntr : 1;
            bool sys : 1;
            bool alu : 1;
            bool fpu : 1;
            bool xmm : 1;
            bool mov : 1;
            bool str : 1;
            bool syn : 1;
        } flags;

        struct Clobber {
            Register *reg;
            std::string why;
        };
        std::vector<Clobber> clobbers;
    protected:

    };
}

#endif
