#ifndef RELENTLESS_INSTR_H
#define RELENTLESS_INSTR_H
#include "Operand.h"

namespace Relentless {
    class InstructionPrototype;

    class Instruction {
    public:
        uint64_t addr;

        InstructionPrototype *instr;
        Operand args[2];

        //std::vector<Operand> reads() const;
        //std::vector<Operand> writes() const;

        std::string mnemonic;
        std::string string;
        inline std::string str() const {
            return mnemonic + " " +string;
        }
    protected:

    };
};

#endif
