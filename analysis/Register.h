#ifndef RELENTLESS_REGISTER_H
#define RELENTLESS_REGISTER_H

namespace Relentless {
    class Register {
    public:
        std::string name, longName;
        struct Span {
            uint16_t begin, uint16_t end;
            inline uint16_t size() const {
                return end - begin;
            }
        } span;

        std::vector<Register*> overlaps;
    protected:

    };
}

#endif
