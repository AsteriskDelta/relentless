#ifndef RELENTLESS_JMP_H
#define RELENTLESS_JMP_H
#include <unordered_map>

namespace Relentless {
    class Node;

    class Jmp {
    public:
        Jmp(Node *f, Node *t);
        Jmp();
        ~Jmp();

        Node *from, *to;
        std::unordered_map<EventIdx, EventTally> tallies;

        void addEvent
    protected:
    };
};

#endif
