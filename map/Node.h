#ifndef RELENTLESS_NODE_H
#define RELENTLESS_NODE_H
#include <unordered_map>

namespace Relentless {
    class Jmp;

    class Node {
    public:
        Node();
        ~Node();

        Jmp* getJump(void *to);

        void *address;
        size_t length;
        std::unordered_map<void*, Jmp*> jumps;

        unsigned char *localData;
    protected:

    };
};

#endif
