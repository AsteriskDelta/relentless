#ifndef RELENTLESS_TRACER_H
#define RELENTLESS_TRACER_H
#include <sys/user.h>

#define X64_MAX_INSTR_LEN 16

namespace Relentless {
    class Tracer {
    public:
        Tracer();
        ~Tracer();

        user_regs_struct registers;

        bool get(void *addr, void *dest, size_t len);
        bool set(void *addr, void *src, size_t len);
        inline bool get(uint64_t addr, void *dest, size_t len) {
            return this->get(reinterpret_cast<void*&>(addr), dest, len);
        }
        inline bool set(uint64_t addr, void *src, size_t len) {
            return this->set(reinterpret_cast<void*&>(addr), src, len);
        }

        void resume();
        void pause();

        void examine();
        void sync();

        void guessInstr();
        void step(bool recursed = false);

        bool attach(pid_t id);
        void detach();

        bool stopped();

        inline pid_t pid() const {
            return pid_;
        }

        void debugRegisters();

        struct {
            struct InstrInfo {
                uint64_t begin, end;
                inline uint64_t size() const {
                    return end - begin;
                }
                unsigned char bytes[X64_MAX_INSTR_LEN];
            };
            InstrInfo previous, current;
        } instr;
        
        static Tracer *Active;
    protected:
        pid_t pid_;
        int status;
    };
};

#endif
