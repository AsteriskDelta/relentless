#include "Patch.h"
#include "Tracer.h"

namespace Relentless {
    std::unordered_map<uint64_t, Patch*> Patch::ByAddr;
    std::unordered_set<Patch*> Patch::ToRestore;

    bool Patch::inject() {
        from.data.resize(to.data.size());
        bool got = Tracer::Active->get(from.addr, &from.data[0], to.data.size());
        return got && Tracer::Active->set(to.addr, &to.data[0], to.data.size());
        ByAddr[to.addr] = this;
    }
    bool Patch::revert() {
        ByAddr.erase(ByAddr.find(to.addr));
        return Tracer::Active->set(from.addr, &from.data[0], from.data.size());
    }
    bool Patch::temporaryRevert() {
        bool ret = this->revert();
        ToRestore.insert(this);
        return ret;
    }

    void Patch::UpdateAll() {
        for(const auto patchPtr : ToRestore) {
            patchPtr->inject();
        }
        ToRestore.clear();
    }
};
