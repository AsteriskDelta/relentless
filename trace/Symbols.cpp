#include "Symbols.h"
#include "Tracer.h"
#include "Symbol.h"
#include <link.h>
#include <elf.h>
#define ELF_HEADER_ADDR 0x400000

namespace Relentless {
    namespace LinkMap {
        uint64_t symtab, strtab;
        link_map *map;
        int nchains;

        /* read data from location addr */
        void read_data(int pid, uint64_t addr, void *vptr, int len)
        {
            _unused(pid);
        	Tracer::Active->get(addr, vptr, len);
        }

        /* read string from pid's memory */
        char *read_str(int pid, uint64_t addr, int len)
        {
        	char *ret = static_cast<char*>(calloc(32, sizeof(char)));
        	read_data(pid, addr, ret, len);
        	return ret;
        }

        /* write data to location addr */
        void write_data(int pid, uint64_t addr, void *vptr, int len)
        {
            _unused(pid);
        	Tracer::Active->set(addr, vptr, len);
        }

        /* locate link-map in pid's memory */
        struct link_map *locate_linkmap(int pid)
        {
        	Elf32_Ehdr *ehdr = static_cast<Elf32_Ehdr*>(malloc(sizeof(Elf32_Ehdr)));
        	Elf32_Phdr *phdr = static_cast<Elf32_Phdr*>(malloc(sizeof(Elf32_Phdr)));
        	Elf32_Dyn *dyn = static_cast<Elf32_Dyn*>(malloc(sizeof(Elf32_Dyn)));
        	Elf32_Word got;
        	struct link_map *l = static_cast<link_map*>(malloc(sizeof(struct link_map)));
        	uint64_t phdr_addr, dyn_addr, map_addr;
        	/*
        	 * first we check from elf header, mapped at 0x08048000, the offset
        	 * to the program header table from where we try to locate
        	 * PT_DYNAMIC section.
        	 */

        	read_data(pid, ELF_HEADER_ADDR, ehdr, sizeof(Elf32_Ehdr));
        	phdr_addr = ELF_HEADER_ADDR + ehdr->e_phoff;
        	printf("program header at %p\n",(void *) phdr_addr);
        	read_data(pid, phdr_addr, phdr, sizeof(Elf32_Phdr));
            sleep(1);
        	while (phdr->p_type != PT_DYNAMIC) {
            sleep(1);
        		read_data(pid, phdr_addr += sizeof(Elf32_Phdr), phdr,
        			  sizeof(Elf32_Phdr));
        	}

        	/*
        	 * now go through dynamic section until we find address of the GOT
        	 */

        	read_data(pid, phdr->p_vaddr, dyn, sizeof(Elf32_Dyn));
        	dyn_addr = phdr->p_vaddr;

        	while (dyn->d_tag != DT_PLTGOT) {
        		read_data(pid, dyn_addr +=
        			  sizeof(Elf32_Dyn), dyn, sizeof(Elf32_Dyn));
        	}

        	got = (Elf32_Word) dyn->d_un.d_ptr;
        	got += 4;	/* second GOT entry, remember? */
        	/*
        	 * now just read first link_map item and return it
        	 */
        	read_data(pid, (uint64_t) got, &map_addr, 4);
        	read_data(pid, map_addr, l, sizeof(struct link_map));
        	free(phdr);
        	free(ehdr);
        	free(dyn);
            map = l;
        	return l;
        }

        /* resolve the tables for symbols*/
        void resolv_tables(int pid)
        {
        	Elf32_Dyn *dyn = static_cast<Elf32_Dyn*>(malloc(sizeof(Elf32_Dyn)));
        	uint64_t addr;
        	addr = (uint64_t) map->l_ld;
        	read_data(pid, addr, dyn, sizeof(Elf32_Dyn));
        	while (dyn->d_tag) {
        		switch (dyn->d_tag) {
        		case DT_HASH:
        			read_data(pid, dyn->d_un.d_ptr +
        				  map->l_addr + 4, &nchains,
        				  sizeof(nchains));
        			break;
        		case DT_STRTAB:
        			strtab = dyn->d_un.d_ptr;
        			break;
        		case DT_SYMTAB:
        			symtab = dyn->d_un.d_ptr;
        			break;
        		default:
        			break;
        		}
        		addr += sizeof(Elf32_Dyn);
        		read_data(pid, addr, dyn, sizeof(Elf32_Dyn));
        	}
        	free(dyn);
        }

        /* find symbol in DT_SYMTAB */
        uint64_t find_sym_in_tables(int pid,
        				 char *sym_name)
        {
        	Elf32_Sym *sym = static_cast<Elf32_Sym*>(malloc(sizeof(Elf32_Sym)));
        	char *str;
        	int i = 0;
        	while (i < nchains) {
        		read_data(pid, symtab + (i * sizeof(Elf32_Sym)), sym,
        			  sizeof(Elf32_Sym));
        		i++;
        		if (ELF32_ST_TYPE(sym->st_info) != STT_FUNC)
        			continue;

        		/* read symbol name from the string table */
        		str = read_str(pid, strtab + sym->st_name, 32);

        		/* compare it with our symbol*/
        		if (strcmp(str, sym_name) == 0) {
        			printf("\nSuccess: got it\n");
        			return (map->l_addr + sym->st_value);
        		}
        	}
        	/* no symbol found, return 0 */
        	printf("\nSorry, No such sym %s\n", sym_name);
        	return 0;
        }

        void load_from_table(Symbols *dest, Tracer *src) {
            Elf32_Sym *sym = static_cast<Elf32_Sym*>(malloc(sizeof(Elf32_Sym)));
        	char *str;
        	int i = 0;

            Tracer *oldTracer = Tracer::Active;
            Tracer::Active = src;
            pid_t pid = 0;

        	while (i < nchains) {
        		read_data(0, symtab + (i * sizeof(Elf32_Sym)), sym,
        			  sizeof(Elf32_Sym));
        		i++;
        		//if (ELF32_ST_TYPE(sym->st_info) != STT_FUNC)
        		//	continue;

        		/* read symbol name from the string table */
        		str = read_str(pid, strtab + sym->st_name, 32);

        		/* compare it with our symbol*/
        		dest->create((map->l_addr+sym->st_value), std::string(str));
        	}

            Tracer::Active = oldTracer;

            free(sym);
        }
    }


    bool Symbols::load(Tracer *tracer) {
        LinkMap::locate_linkmap(0);
        LinkMap::resolv_tables(0);
        LinkMap::load_from_table(this, tracer);
        return true;
    }

    Symbol* Symbols::create(uint64_t addr, const std::string& name) {
        Symbol *sym = this->get(addr, true);
        sym->name = name;
        byName[name] = sym;
        std::cout <<"create sym "<<name <<" at " <<addr <<"\n";
        return sym;
    }

    Symbol* Symbols::get(const std::string& name, bool create) {
        auto it = byName.find(name);
        if(it == byName.end()) {
            if(create) return (byName[name] = new Symbol());
            else return nullptr;
        } else return it->second;
    }
    Symbol* Symbols::get(uint64_t addr, bool create) {
        auto it = byAddr.find(addr);
        if(it == byAddr.end()) {
            if(create) return (byAddr[addr] = new Symbol());
            else return nullptr;
        }
        return it->second;
    }
}
