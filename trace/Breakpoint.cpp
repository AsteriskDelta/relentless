#include "Breakpoint.h"

namespace Relentless {
    static constexpr const uint8_t INT3 = 0xcc, NOP = 0x90;

    void Breakpoint::construct() {
        this->to.data.resize(this->from.data.size());
        for(uint8_t i = 0; i < this->from.data.size(); i++) {
            this->to.data[i] = (i == 0)? INT3 : NOP;
        }
    }
}
