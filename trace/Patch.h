#ifndef RELENTLESS_PATCH_H
#define RELENTLESS_PATCH_H
#include <unordered_map>
#include <set>

namespace Relentless {
    class Patch {
    public:

        struct {
            uint64_t addr;
            std::vector<uint8_t> data;
        } from, to;

        bool active;

        bool inject();
        bool revert();
        bool temporaryRevert();

        static std::unordered_map<uint64_t, Patch*> ByAddr;

        static void UpdateAll();
    protected:
        static std::unordered_set<Patch*> Active;
        static std::unordered_set<Patch*> ToRestore;
    };
}

#endif
