#ifndef RELENTLESS_PTRACE_WRAPPER
#define RELENTLESS_PTRACE_WRAPPER
#include <sys/ptrace.h>
#include <sys/user.h>     /* struct user */
#include <unistd.h>       /* getpid */
#include <errno.h>        /* errno */
#include <stddef.h>       /* offsetof */
#include <stdio.h>        /* perror */
#include <signal.h>       /* kill */
#include <wait.h>         /* waitpid */

template<typename A, typename B, typename C>
int sptrace(__ptrace_request request, A pid, B addr, C data){
    int result;
    errno = 0;
    result = ptrace(request, pid, addr, data);
    if(errno != 0){
        std::cerr <<"\n";
        perror("ptrace");
        throw std::runtime_error("error in ptrace routine");
        //exit(1);
    }else{
        return result;
    }
}
/*
static int pdebug_reg(int pid, int regnum){
    return sptrace(
                PTRACE_PEEKUSER, pid,
                offsetof(struct user, u_debugreg[regnum]), 0
        );
}

static void pset_debug_reg(int pid, int regnum, void *value){
    sptrace(
                PTRACE_POKEUSER, pid,
                offsetof(struct user, u_debugreg[regnum]), (void*)value
        );
}

static int pbit_replace(int old_val, int lsb, int size, int new_val){
    int mask = (-1 << (size+lsb)) | ((1 << lsb) - 1);
    return (old_val & mask) | (new_val << lsb);
}

static void psetup_control_reg(int pid, int regnum, int len, int when){
    int dr7 = pdebug_reg(pid, 7);
    printf("setup_control_reg before: %8.8x\n", dr7);
    dr7 = pbit_replace(dr7, 18 + 4*regnum, 2, len-1);
    dr7 = pbit_replace(dr7, 16 + 4*regnum, 2, when);
    dr7 |= 3 << (2*regnum);
    printf("setup_control_reg after: %8.8x\n", dr7);
    pset_debug_reg(pid, 7, dr7);
}

enum { EXECUTE=0, WRITE=1, READ_WRITE=3 };
*/
#endif
