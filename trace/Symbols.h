#ifndef RELENTLESS_SYMBOLS_H
#define RELENTLESS_SYMBOLS_H
#include <unordered_map>

namespace Relentless {
    class Tracer;
    class Symbol;

    class Symbols {
    public:
        bool load(Tracer *tracer);

        Symbol* get(const std::string& name, bool create = false);
        Symbol* get(uint64_t addr, bool create = false);

        Symbol *create(uint64_t addr, const std::string& name);
    protected:
        std::unordered_map<std::string, Symbol*> byName;
        std::unordered_map<uint64_t, Symbol*> byAddr;
    public:
        struct iterator {
            decltype(byName)::iterator it;

            inline iterator(decltype(byName)::iterator in): it(in) {};
            inline Symbol* operator->() {
                return it->second;
            }
            inline Symbol* operator*() {
                return this->operator->();
            }
            inline auto& operator++() {
                ++it;
                return *this;
            }
            inline bool operator==(const iterator& o) {
                return it == o.it;
            }
        };
        inline auto begin() {
            return iterator(byName.begin());
        }
        inline auto end() {
            return iterator(byName.end());
        }
    };
}

#endif
