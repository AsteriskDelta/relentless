#include "Tracer.h"
#include "ptrace.h"

namespace Relentless {
    Tracer* Tracer::Active = nullptr;
    
    Tracer::Tracer() {
        Active = this;
    }
    Tracer::~Tracer() {

    }

    bool Tracer::get(void *addr, void *dest, size_t len) {
        typedef long word_t;
        errno = 0;
        //    waitpid(pid(), &status, 0);
        //std::cout <<"GET " <<addr <<" -> " << (addr+len) <<"\n";
        try {
            for(size_t off = 0; off < len; off += sizeof(word_t)) {
                word_t word = sptrace(PTRACE_PEEKDATA, pid(), addr + off, nullptr);
                if(errno != 0) return false;
                //std::cout <<"\t" << word <<"\n";
                (*reinterpret_cast<word_t*>(dest + off)) = word;
            }
        } catch(...) {
            return false;
        }
        //std::cout<<"GOT OK\n";

        return true;
    }
    bool Tracer::set(void *addr, void *src, size_t len) {
        typedef long word_t;
        errno = 0;
        //    waitpid(pid(), &status, 0);
        try {
            for(size_t off = 0; off < len; off += sizeof(word_t)) {
                long ret = sptrace(PTRACE_POKEDATA, pid(), addr + off, *reinterpret_cast<word_t*>(src + off));
                _unused(ret);
                if(errno != 0) return false;
            }
        } catch(...) {
            return false;
        }

        return true;
    }

    void Tracer::resume() {
        //waitpid(pid(), &status, WNOHANG);
        //if(WIFSTOPPED(status) == 0) return;
        //waitpid(pid(), &status, 0);
        long ret = sptrace(PTRACE_CONT, pid(), nullptr, nullptr);
        //kill(pid(), SIGCONT);
        //while(WIFSTOPPED(status) != 0) {
            //kill(pid(), SIGCONT);
        //    waitpid(pid(), &status, WUNTRACED | WCONTINUED);
        //}
        _unused(ret);
    }
    void Tracer::pause() {
        //waitpid(pid(), &status, WUNTRACED | WNOHANG);
        //if(WIFSTOPPED(status) != 0) return;
        long ret = sptrace(PTRACE_INTERRUPT, pid(), nullptr, nullptr);
        waitpid(pid(), &status, 0x0);
        //kill(pid(), SIGSTOP);
        /*while(WIFSTOPPED(status) == 0) {
            waitpid(pid(), &status, 0x0);
        }*/
        _unused(ret);
    }

    bool Tracer::stopped(){
        waitpid(pid(), &status, WUNTRACED | WCONTINUED | WNOHANG);
        return WIFSTOPPED(status) != 0;
    }

    void Tracer::debugRegisters() {
        std::cout <<"Registers:\n";
        std::cout <<"\tRIP:\t" << std::hex << registers.rip <<std::dec<<"\n";
        std::cout <<"\tRSP:\t" << std::hex << registers.rsp <<std::dec<<"\n";
        std::cout <<"\tRBP:\t" << std::hex << registers.rbp <<std::dec<<"\n";
        std::cout <<"\tRAX:\t" << std::hex << registers.rax <<std::dec<<"\n";
        std::cout <<"\tRSI:\t" << std::hex << registers.rsi <<std::dec<<"\n";
    }

    void Tracer::examine() {
        /*waitpid(pid(), &status, WUNTRACED | WNOHANG);
        if(WIFSTOPPED(status) == 0) {
            this->pause();
            //waitpid(pid(), &status, 0);
            while(!WIFSTOPPED(status)) waitpid(pid(), &status, WUNTRACED | WNOHANG);
        }*/
        //if(!this->stopped()) this->pause();
        //std::cout <<"examine: wifstop = " << WIFSTOPPED(status)<<"\n";
        //waitpid(pid(), &status, 0);
        long ret = sptrace(PTRACE_GETREGS, pid(), nullptr, &registers);
        //this->debugRegisters();
        _unused(ret);
    }
    void Tracer::sync() {
        //waitpid(pid(), &status, 0);
        long ret = sptrace(PTRACE_SETREGS, pid(), nullptr, &registers);
        _unused(ret);
    }

    void Tracer::step(bool recursed) {
        //waitpid(pid(), &status, WUNTRACED | WNOHANG);
        instr.previous = instr.current;
        uint64_t originalRip = instr.current.begin = this->registers.rip;

        long ret = sptrace(PTRACE_SINGLESTEP, pid(), nullptr, nullptr);
        /*while(WIFSTOPPED(status) == 0) {
            waitpid(pid(), &status, 0x0);
        }*/

        this->pause();
        this->examine();

        if(!recursed) {
            while(this->registers.rip == originalRip) {
                this->step(true);
            }

            //Read the current instruction
            instr.current.end = this->registers.rip;
            //this->pause();
            if(instr.current.size() <= X64_MAX_INSTR_LEN) this->get(instr.current.begin, instr.current.bytes, instr.current.size());
            else instr.current.begin = instr.current.end;
        }

        _unused(ret);
    }

    void Tracer::guessInstr() {
        instr.current.begin = this->registers.rip;
        instr.current.end = this->registers.rip + X64_MAX_INSTR_LEN;
            //this->pause();
        if(true||instr.current.size() <= X64_MAX_INSTR_LEN) this->get(instr.current.begin, instr.current.bytes, instr.current.size());
        //else instr.current.begin = instr.current.end;
    }

    bool Tracer::attach(pid_t id) {
        pid_ = id;
        //ptrace(PTRACE_SETOPTIONS, pid(), 0, PTRACE_O_TRACESYSGOOD);
        long ret = sptrace(PTRACE_SEIZE, pid(), nullptr, nullptr);
        //waitpid(pid(), &status, 0);
        return ret == 0;
    }
    void Tracer::detach() {
        waitpid(pid(), &status, WUNTRACED | WNOHANG);
        if(WIFSTOPPED(status) == 0) {
            this->pause();
            //waitpid(pid(), &status, 0);
            while(!WIFSTOPPED(status)) waitpid(pid(), &status, WUNTRACED | WNOHANG);
        }
        long ret = sptrace(PTRACE_DETACH, pid(), nullptr, nullptr);
        _unused(ret);
    }
}
